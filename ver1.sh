#!/bin/bash
#
#by Tiago França 15 mar 2019
#contato@tiagofranca.com
#

PHP_PATH="/usr/bin/php5.6"
CURRENT_PHP_V="php*"
NEW_PHP_V="php5"
NO_ROOT_MSG="Please run as root"
SERVER_RESTART='service nginx restart'
SERVER_RELOAD='service nginx reload'
SERVER_TEST='nginx -t'
SERVER_SITES_ENABLED='/etc/nginx/sites-enabled/'
HOSTS_FILE="/etc/hosts"

line="===================================================================================="

#Don't modify after here

if [ "$EUID" -ne 0 ]
#  then echo $NO_ROOT_MSG
  then echo -e "\033[05;31m$NO_ROOT_MSG\033[00;37m\e[0m"
  exit
fi


##FUNCOES

#### #Mantem o valor da variavel e reutiliza
#### function algumaVariavel(){
#### algumaVariavel=$algumaVariavel  
#### echo $line
#### read -e -i "$algumaVariavel" -p 'Usuário do banco: ' input
#### algumaVariavel="${input:-$algumaVariavel}"
#### case "$algumaVariavel" in 
####   '' ) algumaVariavel;;
####   n|N ) algumaVariavel="n ou N";;
####   #Asterisco '*' Ação padrão caso nao atenda os parametros, no caso, chama a mesma fgunção
####   * ) algumaVariavel;;
#### esac
#### }

function errMsg(){
## Uso:
## errMsg "Frase 1" "Frase 2 opcional"
par1=${1}
par2=${2}
echo $line
echo -e "\e[1m\e[33m"
errorMsg="ERRO: $par1 $par2"
echo -e "\033[05;101m$errorMsg\033[00;37m\e[0m"
}

function successMsg(){
## Uso:
## successMsg "Frase 1" "Frase 2 opcional"
par1=${1}
par2=${2}
successMsg="$par1 $par2"
echo $line
echo -e "\e[1m\e[32m"
echo -e "\033[1;103m$successMsg\033[00;37m\e[0m"
}

function greenBoldNormal(){
## Uso:
## greenBoldNormal "Verde Forte" "verde normal opcional"
par1=${1}
par2=${2}
echo -e "\e[1m\e[32m\033[01;49m$par1\033[00;37m\e[0m  \e[5m\e[92m\033[05;49m$par2\033[00;37m\e[0m"
}

function redBoldNormal(){
## Uso:
## redBoldNormal "Verde Forte" "verde normal opcional"
par1=${1}
par2=${2}
echo -e "\e[1m\e[31m\033[01;49m$par1\033[00;37m\e[0m  \e[5m\e[91m\033[05;49m$par2\033[00;37m\e[0m"
}

function projectName(){
projectName=$projectName  
echo $line
read -e -i "$projectName" -p "Nome do projeto\n SEM ESPAÇOS, ACENTOS E CARACTERES ESPECIAIS (ex: 'site.local'): " input
projectName="${input:-$projectName}"
case "$projectName" in 
  '' ) projectName;;
esac

case "$projectName" in  
     *\ * ) #Verifica espaços
           errMsg "Contem espaços."
           projectName
          ;;
       *)echo "O nome '$projectName' parece adequado."
        echo "Procure evitar: espaços, caraceteres especiais e acentos.";;
esac

}

function projectUrl(){
projectUrl=$projectName  
echo $line
read -e -i "$projectName" -p "URL local do projeto (SEM PROTOCOLO 'http://' ou 'https://'): "  input
projectUrl="${input:-$projectUrl}"
case "$projectUrl" in 
  '' ) projectUrl;;
esac
}

function protoUrl(){
protoUrl="n"  
echo $line
read -e -i "$protoUrl" -p "Usa HTTPS? (Padrão 'n') [y/n]: " input
protoUrl="${input:-$protoUrl}"
case "$protoUrl" in 
  '' ) protoUrl;;
  y|Y ) protoUrl="https://";;
  n|N ) protoUrl="http://";;
  * ) protoUrl;;
esac
}


function projPath(){
projPath=$projPath  
echo $line
read -e -i "$projPath" -p 'Pasta do projeto (ex: /var/www/site/ ): ' input
projPath="${input:-$projPath}"
case "$projPath" in 
  '' ) projPath;;
esac
testProjPath
}

function createIndexHtmlFile(){
indexFileCont="<!DOCTYPE html>\n
<html>\n
<head>\n
  <meta charset=\"utf-8\">\n
  <title>Novo Site nginx</title>\n
  <style>\n
* {\n
  margin: 0;\n
  padding: 0;\n
  font-size:100%;\n
}\n
html {\n
  font-family: sans-serif;\n
}\n
.container {\n
  width: 60%;\n
  margin: 5% auto auto auto;\n
  padding:2%;\n
  border-radius: 5px;\n
  box-shadow: 1px 3px 3px 3px #00000073;\n
  box-shadow: 0 1px 2px rgba(0,0,0,0.15);\n
  transition: all 0.4s ease-in-out;\n
  cursor:pointer;\n
}\n
\n
.container:hover {\n
  box-shadow: 1px 3px 7px 3px black;\n
  border-radius: 5px;\n
  box-shadow: 0 5px 15px rgba(0,0,0,0.3);\n
}\n
h2 {\n
  font-size: 21pt;\n
  margin-bottom: 2%;\n
}\n
\n
.success {\n
  color: limegreen;\n
  font-style: oblique;\n
  font-size: 130%;\n
}\n
\n
\n
  </style>\n
</head>\n
<body>\n
<div class=\"container\">\n
  <h2>Bem-vindo ao projeto '$projectName'</h2>\n
  <p>Seu novo site foi configurado com <strong class=\"success\">sucesso</strong>!</p>\n
  <p>Esta página refere-se ao arquivo <strong>$projPath/index.html</strong></p>\n
  <p>Todo os arquivos do seu projeto <strong>$projectName</strong> deverá ficar dentro de <strong>$projPath/</strong></p>\n
</div>\n
</body>\n
</html>\n";
echo -e $indexFileCont>$projPath/index.html
chmod 777 -R $projPath/index.html
}

function sureCriateProjPath(){
  mkdir $projPath
  chmod 777 -R $projPath
  chown nobody:nogroup $projPath

  createIndexHtmlFile

if [ -d $projPath ] ; then
  successMsg "Pasta $projPath criada com sucesso"
else #if needed #also: elif [new condition] 
  errMsg "Falha ao criar a pasta $projPath"
fi
}

function testProjPath(){
if [ -d $projPath ] ; then
# echo "A pasta $projPath existe"
echo ""
else #if needed #also: elif [new condition] 
errMsg "A pasta $projPath nao existe"
greenBoldNormal "Deseja criar a pasta no caminho abaixo?" 
redBoldNormal "$projPath"
greenBoldNormal "Y=sim" "(a pasta será criada com permissões 777 -R)"
greenBoldNormal "N=não" "(informar outra)"
read -p '[Y/N]: ' criateProjPath
case "$criateProjPath" in 
  '' ) testProjPath;;
  Y|y ) sureCriateProjPath;;
  N|n ) projPath;;
  * ) testProjPath;;
esac
fi

}

function dbserver(){
dbserver=$dbserver  
echo $line
read -e -i "$dbserver" -p 'Servidor do banco (ex: localhost ou 127.0.0.1): ' input
dbserver="${input:-$dbserver}"
case "$dbserver" in 
  '' ) dbserver;;
esac
}

function dbuser(){
dbuser=$dbuser  
echo $line
read -e -i "$dbuser" -p 'Usuário do banco: ' input
dbuser="${input:-$dbuser}"
case "$dbuser" in 
  '' ) dbuser;;
esac
}

function dbpass(){
dbpass=$dbpass  
echo $line
read -e -i "$dbpass" -p "Senha do banco (se a senha for vazia, digite 'null' sem aspas): " input
dbpass="${input:-$dbpass}"
case "$dbpass" in 
  '' ) dbpass;;
  'null' ) dbpass='';;
esac
}

function dbname(){
dbname=$dbname  
echo $line
read -e -i "$dbname" -p "Nome do banco: " input
dbname="${input:-$dbname}"
case "$dbname" in 
  '' ) dbname;;
  'null' ) dbname='';;
esac
}

function yesNoConfig(){
echo -e "-------------------------------------
| \e[1m CRIAR PROJETO / REVISAR / SAIR \e[21m
-------------------------------------
\e[1m [ C/c ] \e[21m = Criar projeto '$projectName'
\e[1m [ R/r ] \e[21m = Revisar
\e[1m [ X/x ] \e[21m = Sair
";
# choice=$choice
# read -e -i "$choice" -p "[C/R/X]:" input
# choice="${input:-$choice}"
echo $line
read -p "[C/R/X]:" choice
case "$choice" in 
  C|c ) criateConfig;;
  R|r ) callFunctions;;
  X|x ) exitCode;;
  * ) yesNoConfig;;
esac
}

function resumo(){

resumoStr="
 ------------- RESUMO ----------------
 Nome do projeto: $projectName
 URL local do projeto: $protoUrl$projectUrl
 Pasta do projeto: $projPath
 Tipo do projeto: $projType
    ------------------------------------
    | Usuário do banco: $dbuser
    | Senha do banco: $dbpass
    | Nome do banco: $dbname
    |-----------------------------------
-------------------------------------"

IFS=$'\n'       #as novas linhas são separadores
for j in $resumoStr    
do
    echo -e "\033[05;36m$j\033[00;37m\e[0m"
done

yesNoConfig
}


function criateConfig(){
echo $line
read -p 'Escolha Criar[C] Revisar[R] ou Sair[X] 
[C/R/X]: ' criateConfig
case "$criateConfig" in 
  '' ) criateConfig;;
  C|c ) sureCriateConfig;;
  R|r ) callFunctions;;
  X|x ) exitCode;;
  * ) criateConfig;;
esac
}

function projType(){
echo $line
read -p 'Tipo de projeto:
0 - Projeto vazio (PHP Default)
1 - Magento 1.x (PHP 5.6-fpm)
2 - Magento 2.x
3 - Projeto em Branco (PHP 7.1-fpm)
4 - Projeto em Branco (PHP 7.2-fpm)
5 - Projeto em Branco (PHP 5.6-fpm)
[0-5]: ' projType
case "$projType" in 
  '' ) projType;;
  '0' ) projType='outro';;
  '1' ) projType='mage1x';;
  '2' ) projType='mage2x';;
  '3' ) projType='php71fpm';;
  '4' ) projType='php72fpm';;
  '5' ) projType='php56fpm';;
  * ) projType;;
esac

case "$projType" in 
  '' ) projType;;
  'mage1x' ) mageConfigCreate;;
  # 'mage2x' ) 'Magento 2';;
  * ) redBoldNormal "";;
esac

}

function mageConfigCreate(){
echo $line
read -p 'Criar configuração local (local.xml)?
Y - Sim
N - Não
[Y/N]: ' mageConfigCreate
case "$mageConfigCreate" in 
  '' ) mageConfigCreate;;
  'Y'|'y' ) mageConfigCreateTrue;;
  'N'|'n' ) echo '';;
  * ) mageConfigCreate;;
esac
}
function mageConfigCreateTrue(){
localXml="";
localXml+="<?xml version=\"1.0\"?>\n";
localXml+="<config>\n";
localXml+="\t<global>\n";
localXml+="\t\t<install>\n";
localXml+="\t\t\t<date><![CDATA[Wed, 08 Oct 2014 16:32:47 +0000]]></date>\n";
localXml+="\t\t</install>\n";
localXml+="\t\t<crypt>\n";
localXml+="\t\t\t<key><![CDATA[9144bb1c58342ad701945cbe52f4ec3e]]></key>\n";
localXml+="\t\t</crypt>\n";
localXml+="\t\t<disable_local_modules>false</disable_local_modules>\n";
localXml+="\t\t<resources>\n";
localXml+="\t\t\t<db>\n";
localXml+="\t\t\t\t<table_prefix><![CDATA[]]></table_prefix>\n";
localXml+="\t\t\t</db>\n";
localXml+="\t\t\t<default_setup>\n";
localXml+="\t\t\t\t<connection>\n";
localXml+="\t\t\t\t\t<host><![CDATA[$dbserver]]></host>\n";
localXml+="\t\t\t\t\t<username><![CDATA[$dbuser]]></username>\n";
localXml+="\t\t\t\t\t<password><![CDATA[$dbpass]]></password>\n";
localXml+="\t\t\t\t\t<dbname><![CDATA[$dbname]]></dbname>\n";
localXml+="\t\t\t\t\t<initStatements><![CDATA[SET NAMES utf8]]></initStatements>\n";
localXml+="\t\t\t\t\t<model><![CDATA[mysql4]]></model>\n";
localXml+="\t\t\t\t\t<type><![CDATA[pdo_mysql]]></type>\n";
localXml+="\t\t\t\t\t<pdoType><![CDATA[]]></pdoType>\n";
localXml+="\t\t\t\t\t<active>1</active>\n";
localXml+="\t\t\t\t</connection>\n";
localXml+="\t\t\t</default_setup>\n";
localXml+="\t\t</resources>\n";
localXml+="\t<session_save>files</session_save>\n";
localXml+="\t</global>\n";
localXml+="\t<admin>\n";
localXml+="\t\t<routers>\n";
localXml+="\t\t\t<adminhtml>\n";
localXml+="\t\t\t\t<args>\n";
localXml+="\t\t\t\t\t<frontName><![CDATA[admin]]></frontName>\n";
localXml+="\t\t\t\t</args>\n";
localXml+="\t\t\t</adminhtml>\n";
localXml+="\t\t</routers>\n";
localXml+="\t</admin>\n";
localXml+="</config>\n";
if [ -d $projPath/app/ ] ; then
# echo "A pasta $projPath existe"
mkdir -p $projPath/app/etc/
echo ""
else
mkdir -p $projPath/
mkdir -p $projPath/app/
mkdir -p $projPath/app/etc/
fi

mkdir -p $projPath/app/
mkdir -p $projPath/app/etc/
# rm -r $projPath/app/etc/local.xml
echo -e $localXml>$projPath/app/etc/local.xml
chown nobody:nogroup $projPath/app/etc
chown nobody:nogroup $projPath/app/etc/local.xml
chmod 777 -R $projPath/app/etc
}



function exitCode(){
echo $line
read -p 'Tem certeza? Deseja sair? Você perderá as configurações: [Y/N]: ' exitCode
case "$exitCode" in 
  '' ) exitCode;;
  N|n ) resumo;;
  Y|y ) echo "
  Saindo...
  ";
  exit;;
  * ) exitCode;;
esac

}


##FUNCOES EM TESTE

function sureCriateConfig(){
echo 'criating...!'


siteNginx="
server {
\n   listen 80;
\n
\n   server_name $projectUrl;
\n   root $projPath;
\n
\n   index index.html index.php;
\n   location = /js/index.php/x.js {
\n       rewrite ^(.*\.php)/ \$1 last;
\n   }
\n   location / {
\n       try_files \$uri \$uri/ /index.php?\$query_string;
\n   }
\n   location ~ /(app|var|downloader|includes|pkginfo)/ {
\n       deny all;
\n   }
\n
\n   location ~ /\.thumbs { }
\n   location ~ /\. {
\n       deny all;
\n       access_log off;
\n       log_not_found off;
\n   }
\n   location ~* \.(js|css|png|jpg|jpeg|gif|ico)\$ {
\n       expires max;
\n       log_not_found off;
\n       access_log off;
\n       add_header ETag \"\";
\n   }
\n   location ~ \.(hh|php)\$ {
\n       fastcgi_intercept_errors on;
\n       fastcgi_keep_conn on;
\n       fastcgi_pass   unix:/var/run/php/php5.6-fpm.sock;
\n       fastcgi_index  index.php;
\n       fastcgi_param  SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
\n       include        fastcgi_params;
\n   }
\n}
\n";

siteFile=$SERVER_SITES_ENABLED$projectUrl
siteHosts="127.0.0.1 $projectUrl\n"
rm -r $siteFile
touch $siteFile
echo -e $siteNginx>$siteFile
echo -e $siteHosts>>$HOSTS_FILE

echo $line
echo "Testando o Servidor"
restartServerSure
restartServerSure
echo $line
restartServerSure
}

function restartServer(){
echo $line
read -p "Escolha uma opção:
1: REINICIAR o servidor
2: RECARREGAR o servidor sem reiniciar
3: Verificar as configurações ($SERVER_TEST)
4: Testar como  está
5: Farei manualmente depois
[1/2/3/4/5]:" restartServer
case "$restartServer" in 
  '' ) restartServer;;
  1 ) restartServerSure;;
  2 ) reloadServerSure;;
  3 ) checkServer;;
  4 ) openSite;;
  5 ) echo "
  Saindo...
  ";
  exit;;
  * ) exitCode;;
esac
}

function restartServerSure(){
greenBoldNormal "Reiniciando o servidor..."
service nginx reload
service nginx restart
if [ $? -eq 0 ]
then
  greenBoldNormal "Servidor REINICIADO com sucesso!"
else
  redBoldNormal "Falha ao REINICIAR o servidor." >&2
fi
restartServer
}

function checkServer(){
greenBoldNormal "Testando o servidor..."
$SERVER_TEST
# nginx -t
if [ $? -eq 0 ]
then
  greenBoldNormal "Servidor funcionando e todas as configurações estão corretas!"
else
  redBoldNormal "Falha nas configurações do servidor." >&2
fi
restartServer
}

function reloadServerSure(){
greenBoldNormal "Recarregando o servidor..."
service nginx reload
if [ $? -eq 0 ]
then
  greenBoldNormal "Servidor recarregado com sucesso!"
else
  redBoldNormal "Falha ao recarregar o servidor." >&2
fi
restartServer
}

function openSite(){
echo $line
echo "Abra no navegador:
| $protoUrl$projectUrl |
";
echo $line
restartServer
}


function callFunctions(){
projectName
projectUrl
protoUrl
projPath
dbserver
dbuser
dbpass
dbname
projType
resumo
}

##FUNCOES EM TESTE FIM


##FUNCOES END

##EXEC FUNCOES
callFunctions
##EXEC FUNCOES END
