#!/bin/bash

function nome(){
# name=$name
name=dominio.local
read -e -i "$name" -p "Please enter your name: " input
name="${input:-$name}"
case "$name" in  
     *\ * ) #Verifica espaços
			errorSpaces='ERRO: contem espaços'
           echo -e "\033[05;31m$errorSpaces\033[00;37m\e[0m"
           nome
          ;;
       *) echo ""
        echo "O nome '$name' parece adequado."
        echo "Procure evitar: espaços, caraceteres especiais e acentos.";;
esac
}
nome